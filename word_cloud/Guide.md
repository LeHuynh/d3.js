### Word Cloud with Dynamic Masks and Interactions
## Introduction
    This guide explains how to create an interactive word cloud visualization that dynamically changes mask shapes, provides zooming effects, and enhances the user experience with hover interactions. The visualization showcases words in a visually engaging manner.
## Features
    1. Dynamic Masks: The word cloud changes its mask shape after a specified time.
    2. Zooming: The word cloud automatically zooms in and out for a dynamic display.
    3. Hover Effect: When hovering over a word, the word becomes larger and changes color for better visibility and interaction.
    
## Implementation Details
#  Define Data and Configuration
    `data`: An array containing the keywords you want to display in the Word Cloud.
    `colors`: An array containing color codes for each keyword in the Word Cloud.
    `maskFiles`: An array containing paths to mask images used to shape the Word Cloud.

        const wordData = [
          { text: "사랑", color: "#FFA1F5", size: 30 },
          { text: "둘", color: "#0E21A0", size: 20 },
          { text: "아버", color: "#793FDF", size: 25 },
          { text: "엄마", color: "#EA1179", size: 35 },
          { text: "형", color: "#EA1179", size: 10 },
          { text: "언니", color: "#7091F5", size: 22 },
          { text: "나", color: "#F79BD3", size: 33 },
        ];
        const maskFiles = [
          "img/mask-star.png",
          "img/mask-heart.png",
          "img/mask-rhombus.png",
          "img/mask-triangle.png",
          "img/mask-circle.png",
          "img/mask-flower.png",
          "img/mask-cloud.png",
        ];
# Initialize WordCloud
    `newWordCloud`: A variable to store the WordCloud object.
    `function initializeWordCloud()`: This function initializes the WordCloud and sets basic properties such as size, font, and event handling.

      let newWordCloud;
      async function initializeWordCloud() {
        if (!newWordCloud) {
          newWordCloud = new WordCloud(wordData, maskFiles);
        }
        await newWordCloud.updateWordCloud();
        newWordCloud
          .cloud()
          .size([800, 800])
          .data(newWordCloud.generateRandomData(500))
          .mask(newWordCloud.getCurrentMask())
          .padding(2)
          .font("Impact")
          .fontSize((d) => d.size)
          .rotate(() => (~~(Math.random() * 6) - 3) * 30)
          .on("end", function (words) {
            d3.select("div#word-cloud")
              .html("")
              .append("svg")
              .attr("viewBox", "-150 -200 1000 1000")
              .attr("width", 600)

              .append("g")
              .attr(
                "transform",
                `translate(${newWordCloud.cloud().size()[0] / 2},${
                  newWordCloud.cloud().size()[1] / 2
                })`
              )
              .selectAll("text")
              .data(words)
              .enter()
              .append("text")
              .style("font-size", (d) => d.size)
              .style("font-family", "Impact")
              .attr("text-anchor", "middle")
              .attr(
                "transform",
                (d) => "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"
              )
              .text((d) => d.text)
              .style("fill", (d) => {
                const correspondingWord = wordData.find(
                  (word) => word.text === d.text
                );
                return correspondingWord ? correspondingWord.color : "#000";
              })
              .style("background-color", "black")
              .on("mouseover", function (d, i, nodes) {
                newWordCloud.handleMouseOver(d, i, nodes, this);
              })
              .on("mouseout", function (d, i, nodes) {
                newWordCloud.handleMouseOut(d, i, nodes, this);
              });
          })
          .start();
      }
# Update and Display Word Cloud
    The `initializeWordCloud()` function is called to create and display the initial Word Cloud.
    A `setInterval` is used to update the mask and re-display the Word Cloud every 4 seconds.

      initializeWordCloud();
      setInterval(() => {
        newWordCloud.updateMask();
        initializeWordCloud();
      }, 4000);


