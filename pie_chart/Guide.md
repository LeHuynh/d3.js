### Guide for Using PieChart Library

## Introduction
    The PieChart Library is a lightweight, flexible JavaScript library for rendering pie charts based on input data. 
    This library allows you to create dynamic, customizable pie charts to visualize the percentage data of various sectors in a whole.

## Getting Started

# Creating a Container
    Firstly, create a container to house your chart:

        const cont = document.createElement("div");
        cont.style.display = "inline-block";
        cont.style.width = "410px";
        cont.style.height = "400px";
        document.body.appendChild(cont);
# Defining Data
    Define your data in the desired format:

        let dataType = "percentage";
        const data = {
            total: 100,
            array: [
            { key: "At", value: 20 },
            { key: "Dt", value: 21 },
            { key: "At-Dt", value: 15 },
            { key: "At-Dh", value: 30 },
            ],
        };
# Setting Duration
    Set the duration for the animation when the chart is displayed:

        let globalDuration = 400;
        function updateDuration(newDuration) {
            globalDuration = newDuration;
        }
# Creating and Rendering the Chart
    Lastly, create and render your chart with the defined settings:

        const newPie = new PieChart()
            .data(data.array)
            .setTotal(data.total)
            .duration(globalDuration)
            .svgHeight(600)
            .container(cont)
            .render();
            
## API Methods of PieChart Library
# .data(dataArray)
    Take an array of objects containing keys and value to rander the sectors of the chart.
    `dataArray` : Array of objects `{key: string, value: number}`
# .setTotal(totalValue)
    Takes a number to set the total value of the chart.
    `totalValue`: Number
# .duration(durationTime)
    Takes a number to set the animation duration when rendering the chart.
    `durationTime`: Number
# .svgHeught(heightValue)
    Takes a number to set the height of the SVG.
    `heightValue`: Number
# .container(containerElement)
    Takes an HTMl element to set as the container for the chart.
    `containerElement`: HTML Element
# .render()
    Method to draw the chart on the webpage according to the previous setting.