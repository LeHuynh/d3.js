class PieChart {
  constructor(width, height, strokeLineWidth) {
    document
      .getElementsByTagName("head")[0]
      .insertAdjacentHTML(
        "beforeend",
        `<style>.d3-tip { line-height: 1.4; padding: 12px; pointer-events: none !important; color: #203d5d; box-shadow: 0 4px 20px 4px rgba(0, 20, 60, .1), 0 4px 80px -8px rgba(0, 20, 60, .2); background-color: #fff; border-radius: 4px; } /* Creates a small triangle extender for the tooltip */ .d3-tip:after { box-sizing: border-box; display: inline; font-size: 10px; width: 100%; line-height: 1; color: #fff; position: absolute; pointer-events: none; } /* Northward tooltips */ .d3-tip.n:after { content: "▼"; margin: -1px 0 0 0; top: 100%; left: 0; text-align: center; }</style>`
      );
    const attrs = {
      dataTypeValue: "",
      id: "ID" + Math.floor(Math.random() * 1000000),
      svgWidth: width,
      svgHeight: height,
      strokeLineWidth: strokeLineWidth,
      marginTop: 100,
      marginBottom: 100,
      marginRight: 0,
      marginLeft: 500,
      container: "#pie-chart",
      defaultFontSize: 12,
      percentCircleRadius: 14,
      labelMargin: 12,
      defaultTextFill: "#6F68A7",
      backCircleColor: "#EAF0FB",
      defaultFont: "Helvetica",
      valueAccessor: (d) => {
        return d.value;
      },
      round: (d, sum) => Math.round((d.data.value / sum) * 100),
      centerText: ``,
      groupingText: ``,
      valueFormat: (d) => d3.format(".3s")(d),
      ctx: document.createElement("canvas").getContext("2d"),
      dimension: null,
      group: null,
      data: null,
      total: 0,
      setData: (state) => {
        if (!state.group) return state.data;
        const dt = state.group.all();
        dt.sort((a, b) => (a.value > b.value ? -1 : 1));
        return dt;
      },
    };

    Object.assign(attrs, {
      calc: null,
      svg: null,
      chart: null,
      pie: null,
      arc: null,
      arcOuter: null,
    });

    this.getState = () => attrs;
    this.setState = (d) => Object.assign(attrs, d);
    Object.keys(attrs).forEach((key) => {
      this[key] = function (_) {
        var string = `attrs['${key}'] = _`;
        if (!arguments.length) {
          return eval(`attrs['${key}'];`);
        }
        eval(string);
        return this;
      };
    });
    this.initializeEnterExitUpdatePattern();
  }
  duration(value) {
    if (value === undefined) return this._duration;
    this._duration = value;
    return this;
  }
  initializeEnterExitUpdatePattern() {
    d3.selection.prototype.patternify = function (params) {
      var container = this;
      var selector = params.selector;
      var elementTag = params.tag;
      var data = params.data || [selector];
      var selection = container.selectAll("." + selector).data(data, (d, i) => {
        if (typeof d === "object") {
          if (d.id) {
            return d.id;
          }
        }
        return i;
      });
      selection.exit().remove();
      selection = selection.enter().append(elementTag).merge(selection);
      selection.attr("class", selector);
      return selection;
    };
  }

  render() {
    this.setDataProp();
    this.setDynamicContainer();
    this.calculateProperties();
    this.drawSvgAndWrappers();
    this.setLayouts();
    this.drawSlices();
    this.drawCenterTexts();
    this.attachEventHandlers();
    return this;
  }

  setDataProp() {
    const data = this.getData();
    this.setState({ data });
  }
  getTotal() {
    const { total, data } = this.getState();
    if (!total) {
      const total = d3.sum(data, (r) => r.value);
      this.setState({ total: total });
      return total;
    }
    return total;
  }

  setTotal(value) {
    this.setState({ total: value });
    return this;
  }
  drawCenterTexts() {
    const { data, centerPoint, calc, defaultTextFill, centerText } = this.getState();
    const sum = this.getTotal() ?? d3.sum(data, (d) => d.value);
    const percentage = (d) => Math.round((d.value / sum) * 100) + `<span style="font-size:15px;font-weight:bold">%</span> `;
    const fo = centerPoint
      .patternify({ tag: "foreignObject", selector: "center-for-text" })
      .attr("pointer-events", "none")
      .attr("x", -calc.innerRadius)
      .attr("y", -calc.innerRadius)
      .attr("width", calc.innerRadius * 2)
      .attr("height", calc.innerRadius * 2);

    fo.patternify({ tag: "xhtml:div", selector: "for-center-div" }).html(`<div style="height:${
      calc.innerRadius * 2
    }px;display:flex;justify-content:center;align-items:center;text-align:center">
              <div style="display:inline-block;text-transform:uppercase;font-size:25px;font-weight:bold;color:${defaultTextFill}">${centerText} 
                <span style="font-size:35px;font-weight:"bold">${percentage(data[0])}</span> 
              </div>
            </div>`);
  }

  setLayouts() {
    const { calc, data, strokeLineWidth } = this.getState();
    const sum = d3.sum(data, (r) => r.value);
    let total = this.getTotal();
    if (!total || sum >= total) {
      total = sum;
    }

    const pie = d3
      .pie()
      .value((d) => d.value)
      .endAngle((sum / total) * 2 * Math.PI)
      .sort(null);

    const lineWidthOver2 = strokeLineWidth / 2;
    const lineWidthActive = strokeLineWidth;
    const flatArc = d3
      .arc()
      .innerRadius(calc.innerRadius - lineWidthOver2)
      .outerRadius(calc.radius + lineWidthOver2);

    const centerCircle = d3.arc().innerRadius(38).outerRadius(15);

    const activeFlatArc = d3
      .arc()
      .innerRadius(calc.innerRadius - lineWidthActive)
      .outerRadius(calc.radius + lineWidthActive);

    const arc = { flatArc, activeFlatArc, centerCircle };

    const flatArcLable = d3
      .arc()
      .innerRadius(calc.innerRadius - strokeLineWidth)
      .outerRadius(calc.radius + strokeLineWidth);

    const arcOuter = d3.arc().innerRadius(flatArcLable.outerRadius()()).outerRadius(flatArcLable.outerRadius()());

    const arcLabel = d3
      .arc()
      .innerRadius(arcOuter.outerRadius()())
      .outerRadius(arcOuter.outerRadius()() + strokeLineWidth);

    this.setState({ pie, arc, arcOuter, arcLabel });
  }

  attachEventHandlers() {}

  getTextWidth(text, { fontSize = 12, fontWeight = 500 } = {}) {
    const { defaultFont, ctx } = this.getState();
    ctx.font = `${fontWeight || ""} ${fontSize}px ${defaultFont} `;
    const measurement = ctx.measureText(text);
    return measurement.width;
  }

  setDynamicContainer() {
    const attrs = this.getState();
    var container = d3.select(attrs.container);
    var containerRect = container.node().getBoundingClientRect();
    if (containerRect.width > 0) attrs.svgWidth = containerRect.width;
    this.setState({ container });
  }

  drawSvgAndWrappers() {
    const { container, defaultFont, calc, svgWidth, svgHeight } = this.getState();
    const svg = container
      .patternify({
        tag: "svg",
        selector: "svg-chart-container",
      })
      .attr("width", svgWidth)
      .attr("height", svgHeight)
      .attr("font-family", defaultFont);

    svg.append("filter").attr("id", "svg-chart-shadow-filter").attr("color-interpolation-filters", "sRGB").html(`
      <feComponentTransfer in="SourceGraphic">
        <feFuncR type="discrete" tableValues=".5"/>
        <feFuncG type="discrete" tableValues=".5"/>
        <feFuncB type="discrete" tableValues=".5"/>
      </feComponentTransfer>
      <feGaussianBlur stdDeviation="3"/>
      <feOffset dx="0" dy="5" result="shadow"/>
      <feComposite in="SourceGraphic" in2="shadow" operator="over"/>`);

    var chart = svg
      .patternify({
        tag: "g",
        selector: "chart",
      })
      .attr("transform", "translate(" + calc.chartLeftMargin + "," + calc.chartTopMargin + ")");

    const centerPoint = chart
      .patternify({ tag: "g", selector: "center-point" })
      .attr("transform", "translate(" + calc.chartWidth / 4 + "," + calc.chartHeight / 2 + ")");

    this.setState({ chart, svg, centerPoint });
  }

  calculateProperties() {
    const attrs = this.getState();
    var calc = {
      id: "ID" + Math.floor(Math.random() * 1000000), // id for event handlings,
      chartTopMargin: attrs.marginTop,
      chartLeftMargin: attrs.marginLeft,
      chartWidth: null,
      chartHeight: null,
    };
    calc.chartWidth = attrs.svgWidth - attrs.marginRight - calc.chartLeftMargin;
    calc.chartHeight = attrs.svgHeight - attrs.marginBottom - calc.chartTopMargin;
    calc.radius = Math.min(calc.chartWidth, calc.chartHeight) / 2;
    calc.innerRadius = calc.radius * 0.7;
    if (calc.innerRadius < 100) calc.innerRadius = calc.radius * 0.8;

    this.setState({ calc });
  }

  getData() {
    const state = this.getState();
    const { setData } = state;
    return setData(state);
  }

  drawSlices() {
    const { pie, arcLabel, percentCircleRadius } = this.getState();
    const dataInitial = this.getData();
    const sum = this.getTotal() ?? d3.sum(dataInitial, (d) => d.value);
    const minAllowed = 0.04;
    let data = dataInitial.filter((dataItem) => dataItem.value / sum >= minAllowed);
    const others = dataInitial.filter((dataItem) => dataItem.value / sum < minAllowed);
    if (others.length > 1) {
      data.push({
        key: "Others",
        items: others,
        value: d3.sum(others, (d) => d.value),
      });
    } else {
      data = dataInitial;
    }

    const initPieData = pie(data);
    const right = initPieData.filter((d) => this.isRightSide(d));
    const left = initPieData.filter((d) => !this.isRightSide(d));
    initPieData.forEach((d, i, arr) => {
      d.xOffset = 0;
      if ((i != 0 && i != arr.length - 1) || arr.length < 20) {
        d.yOffset = 0;
      } else {
        d.yOffset = -30;
      }
    });

    const process = (d, i, arr) => {
      if (i < 1) return;
      const prev = arr[i - 1];
      const curr = d;

      const yPrev = arcLabel.centroid(prev)[1] + prev.yOffset;
      const yCurr = arcLabel.centroid(curr)[1];
      if (this.isRightSide(curr) && yPrev + percentCircleRadius * 2 > yCurr) {
        curr.yOffset = yPrev + percentCircleRadius * 2 - yCurr + 2;
      } else if (!this.isRightSide(curr) && yPrev + percentCircleRadius * 2 > yCurr) {
        curr.yOffset = yPrev + percentCircleRadius * 2 - yCurr + 2;
        if (arr.length > 4) {
          if (i < 4 + arr.length / 10) {
          }
          if (arr.length > 9) {
            curr.xOffset = 0;
          }
        }
      }
    };
    right.forEach(process);
    left.reverse().forEach(process);
    const tmp = [...initPieData].reverse();
    const firstPoint = { ...tmp.pop() };
    firstPoint.isActive = true;
    tmp.push(firstPoint);
    this.renderPieG(this, tmp, initPieData.reverse());
  }

  renderPieG(that, pieData, initPieData) {
    const { labelMargin, defaultFontSize, centerPoint, arc, calc, arcLabel, defaultTextFill, percentCircleRadius, strokeLineWidth } = that.getState();
    const { flatArc, activeFlatArc, centerCircle } = arc;
    const radius = calc.radius - calc.innerRadius;
    const lineWidthOver2 = strokeLineWidth / 2;
    const lineWidthActive = strokeLineWidth;
    const sum = this.getTotal() ?? d3.sum(pieData, (d) => d.value);
    const renderingPieData = [...pieData].flatMap((d) => {
      const pie = { ...d, type: "path" };
      const startCircle = { ...d, type: "circle", endAngle: d.startAngle };
      const endCircle = { ...d, type: "circle", startAngle: d.endAngle };

      const shadowValue = [{ ...pie }, { ...startCircle }, { ...endCircle }].map((d) => {
        d.isShadow = true;
        return d;
      });
      return [...shadowValue, pie, startCircle, endCircle];
    });
    const pieG = centerPoint.patternify({ tag: "g", selector: "pie-wrapper", data: renderingPieData }).attr("id", (d) => `pie-wrapper-${d.type}`);
    const pieG_Path = pieG.filter((d) => d.type === "path");
    const pieG_Circle = pieG.filter((d) => d.type === "circle");

    pieG_Path
      .patternify({ tag: "path", selector: "pie-paths", data: (d) => [d] })
      .attr("d", (d) => (d.isActive ? activeFlatArc(d) : flatArc(d)))
      .attr("cursor", "pointer")
      .attr("fill", (d) => that.getColor(d));

    pieG_Circle
      .patternify({ tag: "circle", selector: "pie-circle-paths", data: (d) => [d] })
      .attr("cursor", "pointer")
      .attr("cx", (d) => {
        if (d.isActive) {
          return activeFlatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[0];
        }
        return flatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[0];
      })
      .attr("cy", (d) => {
        if (d.isActive) {
          return activeFlatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[1];
        }
        return flatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[1];
      })
      .attr("fill", (d) => that.getColor(d))
      .attr("r", (d) => radius / 2 + (d.isActive ? lineWidthActive : lineWidthOver2));

    pieG_Circle
      .patternify({
        tag: "circle",
        selector: "pie-circle-center-points",
        data: (d) => [d],
      })
      .filter((d) => pieData.find((r) => r.data.key === d.data.key).isActive)
      .attr("cx", (d) => flatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[0])
      .attr("cy", (d) => flatArc.centroid({ startAngle: d.startAngle, endAngle: d.startAngle })[1])
      .style("opacity", this.getLabelOpacity)
      .attr("fill", "#FFFFFF")
      .attr("r", 3);

    pieG
      .selectAll("path:not(.pie-circle-center-points):not(.pie-center-points), circle:not(.pie-circle-center-points):not(.pie-center-points)")
      .filter((d) => d.isShadow)
      .attr("filter", (d) => (d.isActive ? "url(#svg-chart-shadow-filter)" : ""));

    pieG
      .selectAll("path:not(.pie-circle-center-points):not(.pie-center-points), circle:not(.pie-circle-center-points):not(.pie-center-points)")
      .on("mouseenter.color", function (event, d) {
        if (d && d.data && d.data.key) {
          d.active = true;
          d3.selectAll("path:not(.pie-circle-center-points):not(.pie-center-points), circle:not(.pie-circle-center-points):not(.pie-center-points)")
            .filter((r) => r && !r.isShadow && !r.isCenterPoint && r.data && r.data.key === d.data.key)
            .attr("fill", d3.rgb(that.getColor(d)).darker(0.5));
        }
      })
      .on("mouseleave.color", function (event, d) {
        if (d && d.data && d.data.key) {
          d.active = false;
          d3.selectAll("path:not(.pie-circle-center-points):not(.pie-center-points), circle:not(.pie-circle-center-points):not(.pie-center-points)")
            .filter((r) => r && !r.isShadow && !r.isCenterPoint && r.data && r.data.key === d.data.key)
            .attr("fill", that.getColor(d));
        }
      })
      .on("click", function (event, d) {
        const dIndex = initPieData.findIndex((r) => r.data.key === d.data.key);
        const dValue = { ...initPieData[dIndex] };
        dValue.isActive = true;
        const nextPieData = [...initPieData];
        for (let index = dIndex; index < nextPieData.length - 1; index++) {
          nextPieData[index] = nextPieData[index + 1];
        }
        nextPieData.pop();
        nextPieData.push(dValue);
        const dValueDataValue = dValue.data.value;
        if (isNaN(dValueDataValue)) {
          return;
        }
        let centerText = ``;
        const centerDiv = d3.select(".for-center-div").html(`
          <div style="height:${calc.innerRadius * 2}px;display:flex;justify-content:center;align-items:center;text-align:center">
              <div style="display:inline-block;text-transform:uppercase;font-size:30px;font-weight:bold;color:${dValue.data.color}">${centerText}
                  <span style="font-size:35px;font-weight:bold">0</span>
              </div>
          </div>
        `);

        const spanElement = centerDiv.select("span");
        let displayValue = 0;
        data.array.forEach(() => {
          if (newPie.dataTypeValue === "percentage") {
            return (displayValue = dValueDataValue);
          } else if (newPie.dataTypeValue === "count") {
            displayValue = Math.round((dValueDataValue / sum) * 100);
          }
        });

        const countUp = () => {
          spanElement
            .transition()
            .duration(newPie.durationValue)
            .tween("text", function () {
              const i = d3.interpolate(0, displayValue);
              return function (t) {
                displayValue = i(t);
                spanElement.html(Math.round(i(t)) + (newPie.dataTypeValue === "percentage" ? "<span style='font-size:15px;font-weight:bold'>%</span>" : ""));
              };
            });
        };

        countUp();
        that.renderPieG(that, nextPieData, initPieData);
      });

    pieG_Path
      .filter((d) => !d.isShadow)
      .patternify({ tag: "text", selector: "pie-texts", data: (d) => [d] })
      .text((d) => d.data.key)
      .attr("alignment-baseline", "middle")
      .attr("x", (d) => {
        const textMargin = labelMargin + (d.isActive ? 2 : 1) * strokeLineWidth;
        if (this.isRightSide(d)) {
          return arcLabel.centroid(this.correct(d))[0] + textMargin;
        }
        return arcLabel.centroid(this.correct(d))[0] - textMargin;
      })
      .attr("y", (d) => arcLabel.centroid(this.correct(d))[1] + d.yOffset)
      .attr("text-anchor", "middle")
      .attr("font-size", defaultFontSize)
      .attr("fill", "#888")
      .style("opacity", this.getLabelOpacity);
  }

  dataType(value) {
    this.dataTypeValue = value;
    return this;
  }

  duration(value) {
    this.durationValue = value;
    return this;
  }

  updateDuration(newDuration) {
    this.durationValue = newDuration;
  }

  getLabelOpacity(pieItem) {
    if (Math.abs(pieItem.yOffset) > 130) {
      return 0;
    }
    return 1;
  }

  getColor(d) {
    if (!d.data) {
      debugger;
    }
    return d.data.color || "#ccc";
  }

  isRightSide(n) {
    const d = this.correct(n);
    const midAngle = (d.startAngle + d.endAngle) / 2;
    if (midAngle <= Math.PI) return true;
    return false;
  }

  isTopSide(n) {
    const d = this.correct(n);
    const midAngle = (d.startAngle + d.endAngle) / 2;
    if (midAngle <= Math.PI / 2 || midAngle >= (Math.PI * 3) / 2) return true;
    return false;
  }

  correct(d) {
    return Object.assign({}, d, {
      startAngle: d.startAngle,
      endAngle: d.endAngle,
    });
  }

  hashCode(s) {
    for (var i = 0, h; i < s.length; i++) h = (Math.imul(31, h) + s.charCodeAt(i)) | 0;
    return Math.abs(h);
  }

  _doRender() {
    this._doRedraw();
    return this;
  }

  _doRedraw() {
    if (this.hasFilter() && this._multiple) {
    } else if (this.hasFilter()) {
    } else {
    }
    return this;
  }
}
